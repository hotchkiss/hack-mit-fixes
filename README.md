# Hack MIT Page Fix #

For hackmit, the splash had a draggable SVG image which was part of the puzzle.
Unfortunately, this behavior did not work in a few browsers like Mozilla Firefox.
These are the fixes to make it so that it does.

This was during my midterms, so I posted a quick fix as a patch without a full github
PR at first, and after I tested the behavior, I made an additional tweak and submitted.

The partial patch was applied in:
<https://github.com/hotchkiss87/hackmit-splash/commit/37af80587af6987f5b38da0bcb11792ac8718b88>

And the later PR:
<https://github.com/techx/hackmit-splash/pull/17>

Reference to original Issue:
<https://github.com/techx/hackmit-splash/issues/16>
but please excuse the general informality of the original open issue. I was studying and did
not have enough sleep at first!
